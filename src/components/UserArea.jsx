import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import NavBar from './profile/NavBar';
import Profile from './profile/Profile';
import ModalWindow from './ModalWindow';
import { connect } from 'react-redux';

class UserArea extends Component {
  render() {
    const { isModal } = this.props;

    return (
        <div className='user__area'>
          <div className={ `${isModal ? 'modal_open' : ''}` }>
            <NavBar/>
            <Route exact path='/profile' component={ Profile }/>
          </div>
          { isModal && <ModalWindow/> }
        </div>
    )
  }
}

const mapStateToProps = state => ({ isModal: state.user.isModal });

export default connect(mapStateToProps)(UserArea);