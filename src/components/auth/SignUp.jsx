import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form } from 'react-final-form';

import * as actions from '../../actions/auth';
import ModalForm from '../forms/ModalForm';
import { CustomForm } from '../forms/CustomForm';
import { Checkbox } from '../elements/Checkbox';
import { registration } from '../metadata.json';

class SignUp extends Component {
  handleFormSubmit = values => this.props.signUp(values);

  render() {
    const { confirming } = this.props.user;

    return (
        <div className='auth'>
          <ModalForm/>
          <div className={`auth__form ${confirming ? 'auth__form_disabled' : ''} `}>
            <h3 className='auth__title'>Регистрация пользователя</h3>
            <Form onSubmit={ this.handleFormSubmit }
                  render={({ handleSubmit }) => (
                      <CustomForm handleSubmit={ handleSubmit }
                                  fields={ registration }
                                  name='Зарегистрироваться'>
                        <Checkbox/>
                      </CustomForm>)} />
          </div>
        </div>
    )
  }
}

const mapStateToProps = state => ({ user: state.user });

export default connect(mapStateToProps, actions)(SignUp);