import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form } from 'react-final-form';

import * as actions from '../../actions/auth';
import { CustomForm } from '../forms/CustomForm';
import { login } from '../metadata.json';

class SignIn extends Component {
  handleFormSubmit = values => this.props.signIn(values);

  render() {
    return (
        <div className='auth'>
          <h3 className='auth__title'>Вход в Личный кабинет</h3>
          <Form onSubmit={ this.handleFormSubmit }
                render={({ handleSubmit }) => (
                    <CustomForm handleSubmit={ handleSubmit }
                                fields={ login }
                                name='Войти'/>)} />
        </div>
    )
  }
}

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated
});

export default connect(mapStateToProps, actions)(SignIn);