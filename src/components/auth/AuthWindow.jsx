import React from 'react'
import { NavLink, Route } from 'react-router-dom';

import SignUp from './SignUp';
import SignIn from './SignIn';

export const AuthWindow = () =>
    <div className='container_small box__shadow auth__window'>
      <NavLink to='/auth/login' className='auth__link'
               activeClassName='auth__link_active'>
        Войти
      </NavLink>
      <NavLink to='/auth/register' className='auth__link'
               activeClassName='auth__link_active'>
        Регистрация
      </NavLink>

      <Route exact path='/auth/login' component={ SignIn }/>
      <Route path='/auth/register' component={ SignUp }/>
    </div>;