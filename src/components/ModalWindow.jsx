import React, { Component } from 'react'
import { connect } from 'react-redux';

import * as actions from '../actions/user';

class ModalWindow extends Component {
  state = { photo: '' };

  close = () => this.props.closeModal();

  uploadPhoto = event => this.setState({ photo: event.target.value });

  clearPhoto = () => this.setState({ photo: '' });

  render() {
    const { photo } = this.state;

    return (
        <div>
          <div className='modal__block'/>
          <div className='modal__block'>
            <div className='modal__window'>
              <div className='modal__header'>
                <button type='button' onClick={ this.close } className='close'>
                  <span>×</span>
                </button>
                <h4 className='title'>Загрузка новой фотографии</h4>
              </div>
              <div className='modal__body'>
                { photo
                    ?
                    <div className='show'>
                      <img src={ photo }/>
                      <button type='button' className='btn btn_gray'
                              onClick={ this.clearPhoto }>
                        Удалить
                      </button>
                      <button type='submit' className='btn btn__confirm'>
                        Сохранить
                      </button>
                    </div>
                    :
                    <div className='upload'>
                      <label htmlFor='photo'
                             className='btn btn__modal btn_orange'>
                        Выбрать фотографию
                      </label>
                      <input className='input__file' type='file' name='photo'
                             id='photo' accept='.png, .jpg'
                             onChange={ this.uploadPhoto }
                             value={ this.state.photo }/>
                    </div>
                }
              </div>
            </div>
          </div>
        </div>
    )
  }
}

const mapStateToProps = state => ({ info: state.user.info });

export default connect(mapStateToProps, actions)(ModalWindow);