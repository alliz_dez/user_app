import React from 'react'
import { NavLink } from 'react-router-dom';

import { profileNav } from './profileData.json';

export const VerticalMenu = () =>
    <nav className='menu'>
      {
        profileNav.map(item =>
            <li className='menu__item'
                key={ item.link_name }>
              <NavLink to={ `/${item.link_name}` }
                       activeClassName='menu__link_active'
                       className={ `link menu__link
                       ${item.disabled ? 'link_disabled' : ''}` }>
                { item.name }
              </NavLink>
            </li>
        )
      }
    </nav>;