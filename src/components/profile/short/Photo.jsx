import React, { Component } from 'react'
import { connect } from 'react-redux';

import * as actions from '../../../actions/user';
import noAvatar from '../../../assets/images/no_avatar.png';

class Photo extends Component {

  open = () => this.props.openModal();

  render() {

    const { photo } = this.props.data;
    return (
        <div className='photo__area'>
          <img className='photo'
               src={ (photo !== null) ? photo : noAvatar }/>

          <div onClick={ this.open } className='photo__link_wrap'>
            <a href='javascript:void(0)'
               type='button'
               className='link_custom'>
              Изменить фото
            </a>
          </div>
        </div>

    )
  }
}

const mapStateToProps = state => ({ isModal: state.user.isModal });

export default connect(mapStateToProps, actions)(Photo)