import React from 'react';

import { isValue } from '../../../utils/profileUtils';
import Photo from './Photo';

export const ShortInfo = ({ data }) => {
  const { first_name, last_name, middle_name } = data.profile;
  return (
      <div className='box__shadow profile__part profile__short'>
        <Photo data={ data }/>
        <h2 className='profile__title'>
          { `${first_name } ${ last_name } ${ isValue(middle_name)}` }
        </h2>
      </div>
  )
};