import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../../actions/auth';
import { headerNav } from './profileData.json';

class NavBar extends Component {
  handleLogout = () => this.props.signOut();

  render() {
    return (
        <header className='box__shadow'>
          <div className='container nav'>
            <nav className='nav__list'>
              {
                headerNav.map(item =>
                    <li className='nav__item'
                        key={ item.link_name }>
                      <NavLink to={ `/${item.link_name}` }
                               activeClassName='nav__link_active'
                               className={ `link nav__link
                             ${item.disabled ? 'link_disabled' : ''}` }>
                        { item.name }
                      </NavLink>
                    </li>
                )
              }
            </nav>
            <button type='submit' className='btn__logout'
                    onClick={ this.handleLogout }>
              Выйти
            </button>
          </div>
        </header>
    )
  }
}

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated
});

export default connect(mapStateToProps, actions)(NavBar);