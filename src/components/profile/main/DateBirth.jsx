import React  from 'react';

import ModifiedForm from '../../forms/ModifiedForm';

const emptyField = <span className='field_empty'>не выбрана</span>;

export const DateBirth = data => {
  const { date_birth } = data.profile;

  return (
      <div className='field'>
        <div className='field__label'>
          <label>
            Дата рождения
          </label>
        </div>
        <ModifiedForm data={ date_birth ? date_birth : emptyField }>
        </ModifiedForm>
      </div>
  )
};