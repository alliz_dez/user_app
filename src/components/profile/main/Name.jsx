import React from 'react'
import { Field } from 'react-final-form';

import ModifiedForm from '../../forms/ModifiedForm';

export const Name = data => {
  const { first_name, last_name, middle_name } = data.profile;
  const fullName = [ first_name, last_name, middle_name ];
  const spacedFullName = fullName.join(' ');
  return (
      <div className='field'>
        <div className='field__label'>
          <label>
            ФИО
          </label>
        </div>
        <ModifiedForm data={ spacedFullName }>
          <Field component='input' type='text' name='first_name' className='input input__form'/>
          <Field component='input' type='text' name='last_name' className='input input__form'/>
          <Field component='input' type='text' name='middle_name' className='input input__form'/>
        </ModifiedForm>
      </div>
  )
};