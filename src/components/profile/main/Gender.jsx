import React from 'react';
import { Field } from 'react-final-form';

import ModifiedForm from '../../forms/ModifiedForm';

const chooseGender = value => {
  if(value) {
    if(value ===  '**1**') {
      return 'Женский'
    }
    else return 'Mужской'
  }
  return <span className='field_empty'>не выбран</span>
};

export const Gender = data => {
  const { gender } = data.profile;

  return (
      <div className='field'>
        <div className='field__label'>
          <label>
            Пол
          </label>
        </div>
        <ModifiedForm data={ chooseGender(gender) }>
          <Field type='checkbox' name='gender' component='input'/>
        </ModifiedForm>
      </div>
  )
};