import React from 'react';
import { Field } from 'react-final-form';

import ModifiedForm from '../../forms/ModifiedForm';

const emptyField = <span className='field_empty'>не заполнен</span>;

export const Resume = data => {
  const { resume } = data.profile;

  return (
      <div className='field'>
        <div className='field__label'>
          <label>
            Рассказ о себе
          </label>
        </div>
        <ModifiedForm data={ resume ? resume : emptyField }>
          <Field component='textarea' type='text' name='resume' className='input input__textarea'/>
        </ModifiedForm>
      </div>
  )
};
