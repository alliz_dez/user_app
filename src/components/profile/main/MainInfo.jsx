import React from 'react';

import { Name } from './Name';
import { Gender } from './Gender';
import { Resume } from './Resume';
import { DateBirth } from './DateBirth';
import { profileLabels } from '../profileData.json';

export const MainInfo = ({ data }) => {
  const { main } = profileLabels;

  return (
      <div className='box__shadow profile__part'>
        <h3 className='info__title'>{ main.title }</h3>
        <p className='info__text'>{ main.text }</p>
        <Name { ...data }/>
        <Gender { ...data }/>
        <DateBirth { ...data } />
        <Resume { ...data }/>
      </div>
  )
};