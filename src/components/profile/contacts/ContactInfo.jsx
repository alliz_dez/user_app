import React from 'react'

import { CheckedSet } from './CheckedSet';
import { profileLabels } from '../profileData.json';

export const ContactsInfo = ({ data }) => {
  const { email, email_active, phone, phone_active } = data;
  const { contacts } = profileLabels;
  return (
      <div className='box__shadow profile__part'>
        <h3 className='info__title'>{ contacts.title }</h3>
        <p className='info__text'>{ contacts.text }</p>
        <CheckedSet value={ email } active={ email_active } valueName='email'
                    valueType='email' label='Электронная почта'/>
        <CheckedSet value={ phone } active={ phone_active } valueName='phone'
                    valueType='tel' label='Мобильный телефон'/>
      </div>
  )
};