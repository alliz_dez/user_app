import React from 'react';
import { Field } from 'react-final-form';
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faCheckCircle from '@fortawesome/fontawesome-free-solid/faCheckCircle'
import faTimesCircle from '@fortawesome/fontawesome-free-solid/faTimesCircle'

import ModifiedForm from '../../forms/ModifiedForm';

const checked = () => <FontAwesomeIcon icon={ faCheckCircle }
                                       className='icon_checked'/>;

const unChecked = () => <FontAwesomeIcon icon={ faTimesCircle }
                                         className='icon_unchecked'/>;

const fieldSet = (value, active) => {
  return (
      <div className='set__wrap'>
        <span className='set__value'>{ value }</span>
        {
          (active === 0) ? unChecked() : checked()
        }
      </div>
  )
};

export const CheckedSet = ({ value, active, label, valueName, valueType }) => {
  return <div className='field'>
    <div className='field__label'>
      <label>
        { label }
      </label>
    </div>
    <ModifiedForm data={ fieldSet(value, active) }>
      <Field component='input' type={ valueType } name={ valueName }
             className='input input__form'/>
    </ModifiedForm>
  </div>;
};