import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import cookie from 'cookie-machine';

import * as actions from '../../actions/user';
import { VerticalMenu } from './VerticalMenu';
import { ShortInfo } from './short/ShortInfo';
import { MainInfo } from './main/MainInfo';
import { ContactsInfo } from './contacts/ContactInfo';
import { Loading } from '../loading/Loading';

class Profile extends Component {
  componentDidMount() {
    this.props.getInfo();
  }

  render() {
    const data = this.props.info;

    if (data) {
      return (

          <div className='container profile'>
            { !cookie.get('token') && <Redirect to='/auth/login'/> }

            <VerticalMenu/>

            <div className='profile__info'>
              <ShortInfo data={ data }/>
              <MainInfo data={ data }/>
              <ContactsInfo data={ data }/>
            </div>
          </div>
      )
    }
    return <Loading/>
  }
}

const mapStateToProps = state => ({ info: state.user.info });

export default connect(mapStateToProps, actions)(Profile);