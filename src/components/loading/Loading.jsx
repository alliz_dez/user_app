import React from 'react'

export const Loading = () =>
    <div className='load'>
      <div className='load__cube load__cube_1'/>
      <div className='load__cube load__cube_2'/>
      <div className='load__cube load__cube_4'/>
      <div className='load__cube load__cube_3'/>
    </div>;