import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import cookie from 'cookie-machine';

export const AuthRoute = ({ component: Component, ...rest }) =>
    <Route { ...rest } render={ props => (
        cookie.get('token')
            ? <Component { ...props } />
            : <Redirect to={{
              pathname: '/auth',
              state: { from: props.location }
            }} />
    )}/>;