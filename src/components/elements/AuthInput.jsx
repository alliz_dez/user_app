import React from 'react';

export const AuthInput = ({ input, type, placeholder, meta: { touched, error } }) =>
    <div className={ `input__group ${touched && error ? 'error' : ''}` }>
      <input { ...input }
             type={ type }
             placeholder={ placeholder }
             className='input input__auth' autoComplete='new-password'/>
      { touched && error && <div className='form__error'>{ error }</div> }
    </div>;