import React from 'react';
import { Field } from 'react-final-form';

export const Checkbox = () =>
    <label className='input__label'>
      <Field type='checkbox' name='confirm' component='input' className='input__checkbox'/>
      <span>Я ознакомлен(-а) с <a href='#' target='_blank'>
           "Пользовательским лицензионным соглашением"</a></span>
    </label>;