import React from 'react';

export const Error = props =>
    <div>{ props.errorMessage && props.errorMessage.signUp &&
    <div className='error-container'>Oops! { props.errorMessage.signUp }</div> }
    </div>;