import React, { Component } from 'react';
import { Form } from 'react-final-form';
import { connect } from 'react-redux';
import _ from 'lodash'

import * as actions from '../../actions/user';

class ModifiedForm extends Component {
  state = { isModified: false };

  openModification = () => this.setState({ isModified: true });

  closeModification = () => this.setState({ isModified: false });

  handleFormSubmit = values => {
    if (!_.isEmpty(values)) {
      this.props.updateInfo(values);
    }
    this.setState({ isModified: false });
  };

  render() {
    const { isModified } = this.state;
    const { children, data } = this.props;
    return (
        <div className='field__info'>
          {
            !isModified
                ? <div className='data'>
                  <div className='data__wrap'>
                    <span className='field__info'>{ data }</span>
                    <a className='link_custom field__info_hidden'
                        href='javascript:void(0)'
                       onClick={ this.openModification }>
                      Редактировать
                    </a>
                  </div>
                </div>
                : <Form onSubmit={ this.handleFormSubmit }
                        render={ ({ handleSubmit }) => (
                            <form onSubmit={ handleSubmit }>
                              { children }
                              <button type='button'
                                      className='btn btn_gray'
                                      onClick={ this.closeModification }>
                                Отмена
                              </button>
                              <button type='submit'
                                      className='btn btn__confirm'>
                                Сохранить
                              </button>
                            </form>) }/>
          }
        </div>
    )
  }
}

const mapStateToProps = state => ({ info: state.user.info });

export default connect(mapStateToProps, actions)(ModifiedForm);