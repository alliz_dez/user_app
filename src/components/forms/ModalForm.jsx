import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, Form } from 'react-final-form';

import * as actions from '../../actions/user';

class ModalForm extends Component {
  handleCodeSubmit = value => this.props.sendConfirm(value);

  render() {
    const { authenticated } = this.props.auth;
    const { confirming } = this.props.user;
    return (
        <div className='modal__wrapped'>
          {
            (authenticated && confirming) &&
            <Form onSubmit={ this.handleCodeSubmit }
                  render={({ handleSubmit }) => (
                      <form className='modal__form' onSubmit={ handleSubmit }>
                        <h4 className='title modal__title'>
                          Пожалуйста, введите код подтверждения</h4>
                        <Field component='input'
                               type='number'
                               name='activation_code'
                               className='input input__profile input__modal'/>
                        <button type='submit' className='btn btn__confirm'>
                          Подтвердить
                        </button>
                      </form>)} />
          }
        </div>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  user: state.user
});

export default connect(mapStateToProps, actions)(ModalForm);