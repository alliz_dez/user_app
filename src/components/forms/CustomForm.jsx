import React from 'react';
import { Field } from 'react-final-form';

import { AuthInput } from '../elements/AuthInput';

export const CustomForm = ({ children, handleSubmit, fields, name }) =>
    <form className='container_small' onSubmit={ handleSubmit }>
      { fields.map(field =>
          <Field component={ AuthInput }
                 type={ field.type }
                 name={ field.name }
                 key={ field.name }
                 placeholder={ field.placeholder }/>) }
      { children }
      <button type='submit' className='btn btn_orange'>{ name }</button>
    </form>;