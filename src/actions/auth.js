import cookie from 'cookie-machine';
import { replace } from 'react-router-redux';
import { loginUser, registerUser } from '../services/networkService';
import {
  AUTH_FAILURE,
  AUTH_SUCCESS,
  SIGN_UP_FAILURE,
  SIGN_UP_SUCCESS,
  UNAUTH_USER
} from './types';

export const signUp = data => {
  return dispatch => {
    registerUser(data)
    .then(() => {
      dispatch({ type: SIGN_UP_SUCCESS });
      dispatch(signIn(data))
    })
    .catch(response => dispatch({
      type: SIGN_UP_FAILURE, payload: response.error
    }))
  }
};

export const signIn = data => {
  return dispatch => {
    loginUser(data)
    .then(user => {
      cookie.set('token', user.data.token);
      dispatch({ type: AUTH_SUCCESS, payload: user.data.token });
      dispatch(replace('/profile'));
    })
    .catch(response => dispatch({
      type: AUTH_FAILURE, error: response.error
    }))
  }
};

export const signOut = () => {
  return dispatch => {
    cookie.remove('token');
    dispatch({ type: UNAUTH_USER });
    dispatch(replace('/auth/login'))
  }
};