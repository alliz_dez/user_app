export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
export const SIGN_UP_FAILURE = 'SIGN_UP_FAILURE';

export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAILURE = 'AUTH_FAILURE';
export const UNAUTH_USER = 'UNAUTH_USER';

export const SUCCESS = 'SUCCESS';
export const ERROR = 'ERROR';

export const GET_CODE = 'GET_CODE';
export const GET_INFO = 'GET_INFO';
export const CONFIRM_SUCCESS = 'CONFIRM_SUCCESS';
export const UPLOAD_INFO = 'UPLOAD_INFO';
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';