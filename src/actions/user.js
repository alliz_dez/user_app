import {
  GET_CODE, CONFIRM_SUCCESS, UPLOAD_INFO, ERROR, GET_INFO,
  OPEN_MODAL, CLOSE_MODAL
} from './types';
import {
  getActivationCode,
  sendConfirmCode, getInformation,
  updateInformation, updatePhoto
} from '../services/networkService';

export const getCode = () => {
  return dispatch => {
    getActivationCode()
    .then(() => {
      dispatch({ type: GET_CODE });
    })
    .catch(response => dispatch({
      type: ERROR, error: response.error
    }))
  }
};

export const sendConfirm = (data) => {
  return dispatch => {
    sendConfirmCode(data)
    .then(() => {
      dispatch({ type: CONFIRM_SUCCESS })
      //  редирект на профиль
    })
    .catch(response => dispatch({
      type: ERROR, error: response.error
    }))
  }
};

export const getInfo = () => {
  return dispatch => {
    getInformation()
    .then(info => {
      dispatch({ type: GET_INFO, payload: info.data.data });
    })
    .catch(response => dispatch({
      type: ERROR, error: response.error
    }))
  }
};

export const updateInfo = values => {
  return dispatch => {
    updateInformation(values)
    .catch(response => dispatch({
      type: ERROR, error: response.error
    }));
    dispatch({ type: UPLOAD_INFO, payload: values });
  }
};
export const uploadPhoto = value => {
  return dispatch => {
    updatePhoto(value)
    .catch(response => dispatch({
      type: ERROR, error: response.error
    }));
    dispatch({ type: UPLOAD_INFO, payload: value })
  }
};

export const openModal = () => {
  return { type: OPEN_MODAL }
};

export const closeModal = () => {
  return { type: CLOSE_MODAL }
};
