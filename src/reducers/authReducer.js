import {
  AUTH_SUCCESS,
  AUTH_FAILURE,
  SIGN_UP_FAILURE,
  SIGN_UP_SUCCESS,
  UNAUTH_USER
} from '../actions/types';

export default (state = { authenticated: true }, action) => {
  switch (action.type) {
    case SIGN_UP_SUCCESS:
      return { ...state, isLoggedIn: true, error: {} };
    case SIGN_UP_FAILURE:
      return { ...state, isLoggedIn: false, error: {} };
    case AUTH_SUCCESS:
      return { ...state, authenticated: true, token: action.token, error: {} };
    case AUTH_FAILURE:
      return { ...state, authenticated: false, error: {} };
    case UNAUTH_USER:
      return {...state, authenticated: false };
  }
  return state;
}