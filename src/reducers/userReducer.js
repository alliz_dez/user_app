import {
  GET_CODE,
  UPLOAD_INFO,
  UPDATE_PHOTO,
  CONFIRM_SUCCESS, GET_INFO, OPEN_MODAL, CLOSE_MODAL
} from '../actions/types';

export default (state = { info: null, isModal: false }, action) => {
  switch (action.type) {
    case GET_CODE:
      return { ...state, confirming: true };
    case CONFIRM_SUCCESS:
      return { ...state };
    case GET_INFO:
      return { ...state, info: action.payload };
    case UPLOAD_INFO: {
      const newInfo = processProperty(state.info, action.payload);
      return { ...state, info: newInfo };
    }
    case OPEN_MODAL:
      return { ...state, isModal: true };
    case CLOSE_MODAL:
      return { ...state, isModal: false };
  }
  return state;
}

function processProperty(value, property) {
  const result = Object.assign({}, value);
  Object.keys(property).forEach(key => {
    const propertyElement = property[key];
    if (result[key] !== undefined) {
      result[key] = propertyElement;
    } else {
      if (result['profile'][key] !== undefined) {
        result['profile'][key] = propertyElement
      }
    }
  });
  return result;
}