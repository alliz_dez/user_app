import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

import { App } from './containers/App';
import reducers from './reducers';

import './style/main.scss';

const history = createHistory();

const createStoreWithMiddleware = applyMiddleware(
  routerMiddleware(history),
  reduxThunk
);

const store = createStore(
    reducers,
    {},
    createStoreWithMiddleware
);

ReactDOM.render(
    <Provider store={ store }>
      <ConnectedRouter history={ history }>
        <App/>
      </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);