import React, { Fragment } from 'react';
import { Route, Redirect } from 'react-router-dom';
import UserArea from '../components/UserArea';
import { AuthRoute } from '../components/elements/AuthRoute';
import { AuthWindow } from '../components/auth/AuthWindow';

export const App = () => (
  <Fragment>
    <AuthRoute exact path='/profile' component={ UserArea } />
    <Route path='/auth' component={ AuthWindow } />
    <Redirect to='/profile' />
  </Fragment>
);