import { instance } from './axiosConfig';

export function registerUser(user) {
  return instance.post('/auth/register', user);
}

export function loginUser(user) {
  return instance.post('/auth/login', user);
}

export function getActivationCode() {
  return instance.get('/user/code_activation');
}

export function sendConfirmCode(code) {
  return instance.post('/user/confirm_phone', code);
}

export function getInformation() {
  return instance.get('/profile/information');
}

export function updateInformation(info) {
  return instance.put('/profile/information', info);
}

export function updatePhoto() {
  return instance.post('/profile/photo');
}

export function createDocument(type) {
  return instance.post(`/profile/document/${type}`);
}

export function deleteDocument(type) {
  return instance.delete(`/profile/document/${type}`);
}

export function getDocuments() {
  return instance.delete('/profile/document');
}