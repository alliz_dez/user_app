import axios from 'axios';
import cookie from 'cookie-machine';

const BASE_URL = 'http://ih594954.vds.myihor.ru:8000';

export const instance = axios.create({
  baseURL: BASE_URL,
  // headers: { 'Authorization': `Bearer ${TOKEN}` }
});

instance.interceptors.request.use((config) => {
  const token = cookie.get('token');

	if (token) {
    config.headers.common['Authorization'] = `Bearer ${ token }`
  }

  return config
})