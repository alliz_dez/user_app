const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

module.exports = {
  devtool: 'source-map',
  entry: [
    path.join(__dirname, '/src/index'),
  ],
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        include: [
          path.resolve(__dirname, './src')
        ]
      }, {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          'url-loader?limit=10000',
          'img-loader'
        ]
      }, {
        test: /\.scss$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader'
        }, {
          loader: 'postcss-loader',
          options: {
            plugins: [
              autoprefixer({
                browsers:['ie >= 8', 'last 4 version']
              })
            ],
            sourceMap: false
          }
        }, {
          loader: 'sass-loader',
          options: {
            includePaths: ['absolute/path/a', 'absolute/path/b']
          }
        }]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin
    ({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      },
    })
  ],

  devServer: {
		historyApiFallback: true,
		stats: 'errors-only'
	}
};